# Inspec test for recipe elastic-repo::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

if os.family == 'debian'
  describe apt('https://artifacts.elastic.co/packages/7.x/apt') do
    it { should exist }
    it { should be_enabled }
  end

  describe apt('https://artifacts.elastic.co/packages/6.x/apt') do
    it { should_not exist }
  end
end

if os.family == 'redhat'
  describe yum.repo('elastic-7.x') do
    it { should exist }
    it { should be_enabled }
  end

  describe yum.repo('elastic-6.x') do
    it { should_not exist }
  end
end
