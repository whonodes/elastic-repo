return unless os.family == 'debain'

nodes_dir = '/tmp/kitchen/nodes'
tk_instance = command("ls #{nodes_dir}").stdout.strip
node_json = "#{nodes_dir}/#{tk_instance}"
node = json(node_json).params

if node['normal']['elastic']['repo']['7.x'] == 'true'
  describe apt('https://artifacts.elastic.co/packages/7.x/apt') do
    it { should exist }
    it { should be_enabled }
  end
else
  describe apt('https://artifacts.elastic.co/packages/7.x/apt') do
    it { should_not exist }
  end
end

if node['normal']['elastic']['repo']['6.x'] == 'true'
  describe apt('https://artifacts.elastic.co/packages/6.x/apt') do
    it { should exist }
    it { should be_enabled }
  end
else
  describe apt('https://artifacts.elastic.co/packages/6.x/apt') do
    it { should_not exist }
  end
end
