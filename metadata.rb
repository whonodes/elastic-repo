name 'elastic-repo'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures elastic-repo'
version '0.1.2'
chef_version '>= 14.0'

%w(centos fedora debian redhat ubuntu).each do |os|
  supports os
end

issues_url 'https://bitbucket.org/whonodes/elastic-repo/issues'
source_url 'https://bitbucket.org/whonodes/elastic-repo/src'
