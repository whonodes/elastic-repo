# Used to create a yum repo entry

if node['elastic']['repo']['7.x']
  yum_repository 'elastic-7.x' do
    description 'Elasticsearch Yum Repo'
    baseurl 'https://artifacts.elastic.co/packages/7.x/yum'
    gpgkey 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
    action :create
    only_if { node['elastic']['repo']['7.x'] }
  end
else
  yum_repository 'elastic-7.x' do
    action :remove
  end
end

if node['elastic']['repo']['6.x']
  yum_repository 'elastic-6.x' do
    description 'Elasticsearch Yum Repo'
    baseurl 'https://artifacts.elastic.co/packages/6.x/yum'
    gpgkey 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
    action :create
    only_if { node['elastic']['repo']['6.x'] }
  end
else
  yum_repository 'elastic-6.x' do
    action :remove
  end
end
