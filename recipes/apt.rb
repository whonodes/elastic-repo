# APT Repo

apt_package 'apt-transport-https' do
  action :install
end

apt_repository 'elastic-7.x' do
  uri 'https://artifacts.elastic.co/packages/7.x/apt'
  key 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
  components ['main']
  distribution 'stable'
  action :add
  deb_src false
  only_if { node['elastic']['repo']['7.x'] }
end

apt_repository 'elastic-7.x' do
  action :remove
  not_if { node['elastic']['repo']['7.x'] }
end

apt_repository 'elastic-6.x' do
  uri 'https://artifacts.elastic.co/packages/7.x/apt'
  key 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
  components ['main']
  distribution 'stable'
  action :add
  deb_src false
  only_if { node['elastic']['repo']['6.x'] }
end

apt_repository 'elastic-6.x' do
  action :remove
  not_if { node['elastic']['repo']['6.x'] }
end

apt_repository 'elasticsearch' do
  action :remove
end

apt_repository 'elastic' do
  action :remove
end
