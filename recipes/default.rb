#
# Cookbook:: elastic-repo
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe 'elastic-repo::apt' if platform_family?('debian')
include_recipe 'elastic-repo::yum' if platform_family?('rhel')
