# Default Attributes

node.default['elastic']['repo']['7.x'] = true
node.default['elastic']['repo']['6.x'] = false
node.default['elastic']['repo']['package_url'] = 'https://artifacts.elastic.co/downloads/'
